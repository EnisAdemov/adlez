/// <reference path = "../lib/phaser.d.ts"/>
/// <reference path = "../gameObjects/Player.ts"/>
/// <reference path = "../gameObjects/ParticleEffect.ts"/>
/// <reference path = "../gameObjects/ParticleEffectEmitter.ts"/>
var Adlez;
(function (Adlez) {
    class Game extends Phaser.Stage {
        create() {
            this.player = new Adlez.Player(this.game);
            // this.emitter = new ParticleEffectEmitter(this.game, 100, 600);
        }
        update() {
        }
        render() {
            this.game.debug.pointer(this.game.input.activePointer);
        }
    }
    Adlez.Game = Game;
})(Adlez || (Adlez = {}));
//# sourceMappingURL=Game.js.map
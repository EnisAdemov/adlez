/// <reference path = "../lib/phaser.d.ts"/>
/// <reference path = "../gameObjects/Player.ts"/>
/// <reference path = "../gameObjects/ParticleEffect.ts"/>
/// <reference path = "../gameObjects/ParticleEffectEmitter.ts"/>


module Adlez {
    export class Game extends Phaser.Stage {
        player: Player;
        emitter: ParticleEffectEmitter;

        create() {

            this.player = new Player(this.game);
           // this.emitter = new ParticleEffectEmitter(this.game, 100, 600);
        }

        update() {

        }

        render() {
            this.game.debug.pointer( this.game.input.activePointer );
        }

    }
}
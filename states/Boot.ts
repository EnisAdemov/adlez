/// <reference path = "../lib/phaser.d.ts"/>

module Adlez {
    export class Boot extends Phaser.State {

        preload() {

        }

        create() {
            this.game.state.start("Game");
        }
    }
}
/// <reference path = "../lib/phaser.d.ts"/>
var Adlez;
(function (Adlez) {
    class Boot extends Phaser.State {
        preload() {
        }
        create() {
            this.game.state.start("Game");
        }
    }
    Adlez.Boot = Boot;
})(Adlez || (Adlez = {}));
//# sourceMappingURL=Boot.js.map
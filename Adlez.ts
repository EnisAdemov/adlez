/// <reference path = "lib/phaser.d.ts"/>
/// <reference path = "states/Boot.ts"/>
/// <reference path = "states/Game.ts"/>
/// <reference path = "states/Menu.ts"/>
/// <reference path = "gameObjects/Platform.ts"/>
/// <reference path = "gameObjects/Player.ts"/>
/// <reference path = "gameObjects/Blocks.ts"/>
/// <reference path = "gameObjects/Score.ts"/>
/// <reference path = "gameObjects/ParticleEffect.ts"/>
/// <reference path = "gameObjects/ParticleEffectEmitter.ts"/>


module Adlez {
    class Adlez extends Phaser.Game {
        game:Phaser.Game;

        constructor(width?:number, height?:number) {
            super(width, height, Phaser.AUTO, 'phaser-div');

            this.state.add("Boot", Boot, false);
            this.state.add("Menu", Menu, false);
            this.state.add("Game", Game, false);
            this.state.start("Boot");
        }
    }

    window.onload = ()=> {
        new Adlez(1024, 720);
    }
}

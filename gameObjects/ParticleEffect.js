/// <reference path = "../lib/phaser.d.ts"/>
var Adlez;
(function (Adlez) {
    class ParticleEffect extends Phaser.Particle {
        constructor(game, x, y) {
            this.game = game;
            this.radius = 5 + Math.round(Math.random() * (5 - 0) + 0);
            super(this.game, x, y, game.cache.getBitmapData("explode"));
            this.x = x;
            this.y = y;
            this.createExplode();
        }
        update() {
            if (this.alpha > 0.01) {
                this.alpha = this.alpha - Math.round(Math.random() * (0.009 - 0.005) + 0.005);
            }
            super.update();
        }
        createExplode() {
            var bmd = this.game.add.bitmapData(this.radius * 2, this.radius * 2);
            bmd.circle(bmd.width * 0.5, bmd.height * 0.5, this.radius, "#99c24d");
            this.game.cache.addBitmapData("explode", bmd);
        }
    }
    Adlez.ParticleEffect = ParticleEffect;
})(Adlez || (Adlez = {}));
//# sourceMappingURL=ParticleEffect.js.map
/// <reference path="../lib/phaser.d.ts"/>
///<reference path="ParticleEffect.ts"/>
var Adlez;
(function (Adlez) {
    class ParticleEffectEmitter {
        constructor(game, width, gravity) {
            this.game = game;
            this.emitter = this.game.add.emitter(0, 0, 0);
            this.emitter.particleClass = Adlez.ParticleEffect;
            this.emitter.width = width;
            this.emitter.makeParticles("explode");
            this.emitter.setAlpha(0.8, 0.3);
        }
        start(x, y, explode, lifespan, quantity) {
            this.emitter.x = x;
            this.emitter.y = y;
            this.emitter.start(explode, lifespan, 0, quantity, true);
        }
    }
    Adlez.ParticleEffectEmitter = ParticleEffectEmitter;
})(Adlez || (Adlez = {}));
//# sourceMappingURL=ParticleEffectEmitter.js.map
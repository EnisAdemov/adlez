/// <reference path = "../lib/phaser.d.ts"/>

/*
 left ->  90 600
 mid ->   440, 600
 right -> 800, 600
 speed -> 350
 */

module Adlez {
    export class Player {
        game:Phaser.Game;
        cirPlayer:Phaser.Graphics;
        leftKey:Phaser.Key;
        rightKey:Phaser.Key;

        constructor(game:Phaser.Game) {
            this.game = game;
            this.game.physics.startSystem(Phaser.Physics.ARCADE);

            //drawing circle and some physics
            this.cirPlayer = this.game.add.graphics(0, 0);
            this.cirPlayer.beginFill(0x99c24d);
            this.cirPlayer.drawCircle(40, 40, 90); //relative possition
            this.game.physics.enable(this.cirPlayer, Phaser.Physics.ARCADE);
            this.cirPlayer.position.set(this.game.world.centerX, this.game.world.centerY);

            //Player Input
            this.leftKey = this.game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
            this.rightKey = this.game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
        }

        update() {

        }

    }
}
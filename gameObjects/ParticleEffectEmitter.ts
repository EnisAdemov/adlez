/// <reference path="../lib/phaser.d.ts"/>
///<reference path="ParticleEffect.ts"/>

module Adlez{
    export class ParticleEffectEmitter{
        game:Phaser.Game;
        emitter:Phaser.Particles.Arcade.Emitter;

        constructor(game:Phaser.Game, width:number, gravity:number){
            this.game = game;
            this.emitter = this.game.add.emitter(0,0,0);
            this.emitter.particleClass = ParticleEffect;
            this.emitter.width = width;
            this.emitter.makeParticles("explode");
            this.emitter.setAlpha(0.8,0.3);
        }

        start(x:number, y:number, explode:boolean, lifespan:number, quantity:number){
            this.emitter.x = x;
            this.emitter.y = y;
            this.emitter.start(explode, lifespan,0,quantity,true);
        }
    }
}
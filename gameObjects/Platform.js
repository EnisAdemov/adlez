/// <reference path = "../lib/phaser.d.ts"/>
var Adlez;
(function (Adlez) {
    class PlayerTank {
        constructor(game) {
            this.game = game;
            this.backgroundColor = "#2f2d2e";
            this.createPlatform();
        }
        createPlatform() {
            this.game.stage.backgroundColor = this.backgroundColor;
            this.platLine = this.game.add.sprite(0, 0, 'lines');
            this.platLine.tint = 0xf18f01;
        }
    }
    Adlez.PlayerTank = PlayerTank;
})(Adlez || (Adlez = {}));
//# sourceMappingURL=Platform.js.map
/// <reference path = "../lib/phaser.d.ts"/>


module Adlez {

    export enum blockLane  { LEFT, MID, RIGHT };

    export class Blocks {
        game: Phaser.Game;
        recBlock: Phaser.Graphics;
        recGroup: Phaser.Group;
        setTimer: Phaser.Timer; //setting the timer
        recSpeed: number = 3000;
        spawnTime: number = 1000; //frequency on spawing

        constructor(game:Phaser.Game) {
            this.game = game;
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.recGroup = this.game.add.group();

            //setting the timer for spawn
            this.setTimer = game.time.create(false);
            this.setTimer.loop(this.spawnTime, this.spawnBlock, this);
            this.setTimer.start();

        }

        update() {

        }

        getRandomLane() {
            return Math.floor(Math.random() * Object.keys(blockLane).length/2);
        }

        spawnBlock() {
            //drawing rectangles and some physics
            this.recBlock = this.game.add.graphics(470, 0);
            this.recBlock.beginFill(0x1cc24d);
            this.recBlock.drawRect(0, 0, 1, 1); //relative

            this.recGroup.add(this.recBlock); //add to group
            this.game.physics.enable(this.recGroup, Phaser.Physics.ARCADE);

            if(this.getRandomLane() == blockLane.MID) {
                this.game.add.tween(this.recBlock.body).to({x: 470}, this.recSpeed, Phaser.Easing.Linear.None, true, 0, 0);
            } else if(this.getRandomLane() == blockLane.LEFT) {
                this.game.add.tween(this.recBlock.body).to({x: -50}, this.recSpeed, Phaser.Easing.Linear.None, true, 0, 0);
            } else if(this.getRandomLane() == blockLane.RIGHT) {
                this.game.add.tween(this.recBlock.body).to({x: 1000}, this.recSpeed, Phaser.Easing.Linear.None, true, 0, 0);
            }
            this.game.add.tween(this.recBlock.body).to({y: 820}, this.recSpeed, Phaser.Easing.Linear.None, true, 0, 0);
            this.game.add.tween(this.recBlock.scale).to({x: 150, y: 150}, this.recSpeed, Phaser.Easing.Linear.None, true, 0, 0);
        }

    }
}
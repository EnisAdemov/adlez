/// <reference path = "../lib/phaser.d.ts"/>

module Adlez {
    export class PlayerTank {
        game:Phaser.Game;
        backgroundColor: string;
        platLine: Phaser.Sprite;
        graphics: Phaser.Graphics;

        constructor(game:Phaser.Game) {
            this.game = game;
            this.backgroundColor = "#2f2d2e";

            this.createPlatform();
        }

        createPlatform() {
            this.game.stage.backgroundColor = this.backgroundColor;
            this.platLine = this.game.add.sprite(0, 0, 'lines');
            this.platLine.tint = 0xf18f01;
        }
    }
}
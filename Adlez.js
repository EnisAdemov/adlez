/// <reference path = "lib/phaser.d.ts"/>
/// <reference path = "states/Boot.ts"/>
/// <reference path = "states/Game.ts"/>
/// <reference path = "states/Menu.ts"/>
/// <reference path = "gameObjects/Platform.ts"/>
/// <reference path = "gameObjects/Player.ts"/>
/// <reference path = "gameObjects/Blocks.ts"/>
/// <reference path = "gameObjects/Score.ts"/>
/// <reference path = "gameObjects/ParticleEffect.ts"/>
/// <reference path = "gameObjects/ParticleEffectEmitter.ts"/>
var Adlez;
(function (Adlez_1) {
    class Adlez extends Phaser.Game {
        constructor(width, height) {
            super(width, height, Phaser.AUTO, 'phaser-div');
            this.state.add("Boot", Adlez_1.Boot, false);
            this.state.add("Menu", Adlez_1.Menu, false);
            this.state.add("Game", Adlez_1.Game, false);
            this.state.start("Boot");
        }
    }
    window.onload = () => {
        new Adlez(1024, 720);
    };
})(Adlez || (Adlez = {}));
//# sourceMappingURL=Adlez.js.map